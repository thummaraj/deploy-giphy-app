import React, {Component} from 'react'
import { Card } from 'antd'
import { connect } from 'react-redux'
const {Meta} = Card;

const mapDispathToProps = dispath => {
  return {
      onItemGiphyClick: item => 
      dispath({
          type:'click_item',
          payload: item
      })
  }
}
class ItemFavorite extends Component{
  
    render() {
        const item = this.props.item
        return (
          <Card 
          onClick={ () => {
            this.props.onItemGiphyClick(item);
          }}
          style={{ height: 200, width: 200}}
          
          
          hoverable
          cover={< img src={item.images.fixed_width.url} />}
          >
          <Meta
          title={item.title}
          />
          </Card>
        )
    }
}
export default connect(
  null,
  mapDispathToProps
)(ItemFavorite);
