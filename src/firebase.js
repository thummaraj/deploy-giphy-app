import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyCh0z6S8iKb7ljHyFSvmcQwYYXpYogKlF0",
    authDomain: "workshop-dv-8b83d.firebaseapp.com",
    databaseURL: "https://example-everything.firebaseio.com",
    projectId: "workshop-dv-8b83d",
    storageBucket: "example-everything.appspot.com",
    messagingSenderId: "793947592819"
}

firebase.initializeApp(config)

const database = firebase.database()
const auth = firebase.auth()
const provider = new firebase.auth.FacebookAuthProvider()

export {
    database,
    auth,
    provider
}